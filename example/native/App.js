import React from 'react';
import styled from 'styled-components';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import {
	AnimateMove,
	AnimateRotate,
	AnimateScale,
	AnimatedPosition,
	AnimatedRotation,
	AnimatedScale,
} from 'react-declare-animate';

const WholeScreen = styled.View`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 30px;
`;

const ButtonArea = styled.View`
	display: flex;
	flex-direction: row;
`;

const Button = ({ onPress, title, style = {} }) => (
	<TouchableWithoutFeedback onPress={onPress}>
		<View style={{ backgroundColor: 'black', width: 40, ...style }}>
			<Text style={{ color: 'white', textAlign: 'center' }}>{title}</Text>
		</View>
	</TouchableWithoutFeedback>
);

const Presentation = styled.View`
	display: flex;
	flex-direction: column;
`;

const AnimatedStates = styled.View`
	position: relative;
	width: 100%;
	height: 300px;
`;

const AnimateActions = styled.View`
	position: relative;
	width: 100%;
	height: 300px;
	display: flex;
	flex-direction: column;
	align-items: center;
`;

export default class App extends React.Component {
	constructor(...args) {
		super(...args);
		this.state = {
			top: 100,
			left: 100,
			angle: 0,
			scale: 2,
			runActions: false,
		};
	}

	shiftUp = () => this.setState({ top: this.state.top - 80 });
	shiftDown = () => this.setState({ top: this.state.top + 80 });
	shiftLeft = () => this.setState({ left: this.state.left - 80 });
	shiftRight = () => this.setState({ left: this.state.left + 80 });
	cw = () => this.setState({ angle: this.state.angle + 30 });
	ccw = () => this.setState({ angle: this.state.angle - 30 });
	zoomIn = () => this.setState({ scale: this.state.scale * 2 });
	zoomOut = () => this.setState({ scale: this.state.scale / 2 });
	toggleAnimateActions = () => this.setState({ runActions: !this.state.runActions });

	render() {
		return (
			<WholeScreen>
				<ButtonArea>
					<Button onPress={this.shiftLeft} title="←" />
					<Button onPress={this.shiftUp} title="↑" />
					<Button onPress={this.shiftDown} title="↓" />
					<Button onPress={this.shiftRight} title="→" />
					<Button onPress={this.cw} title="↻" />
					<Button onPress={this.ccw} title="↺" />
					<Button onPress={this.zoomIn} title="+" />
					<Button onPress={this.zoomOut} title="-" />
				</ButtonArea>

				<Presentation>
					<AnimatedStates>
						<AnimatedPosition duration={2000} top={this.state.top} left={this.state.left}>
							<AnimatedScale duration={2000} scaleX={this.state.scale} scaleY={this.state.scale}>
								<AnimatedRotation duration={2000} angleZ={this.state.angle}>
									<Text>What is This?</Text>
								</AnimatedRotation>
							</AnimatedScale>
						</AnimatedPosition>
					</AnimatedStates>
					<AnimateActions>
						<Button
							onPress={this.toggleAnimateActions}
							title={this.state.runActions ? 'Unmount' : 'Run'}
							style={{ width: 100 }}
						/>
						{this.state.runActions && (
							<AnimateMove duration={2000} start={{ top: 50, left: 30 }} end={{ top: this.state.top, left: this.state.left }}>
								<AnimateScale duration={2000} start={{ X: 1, Y: 1}} end={{ X: this.state.scale, Y: this.state.scale}}>
									<AnimateRotate duration={2000} start={{ Z: 10 }} end={{ Z: this.state.angle }}>
										<Text>Who are You?</Text>
									</AnimateRotate>
								</AnimateScale>
							</AnimateMove>
						)}
					</AnimateActions>
				</Presentation>
			</WholeScreen>
		);
	}
}