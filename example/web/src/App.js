import React from 'react';
import styled from 'styled-components';
import {
	AnimateMove,
	AnimateRotate,
	AnimateScale,
	AnimatedPosition,
	AnimatedRotation,
	AnimatedScale,
} from 'react-declare-animate'

const WholeScreen = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	padding: 30px;
	align-items: center;
`;

const ButtonArea = styled.div`
	display: flex;
	flex-direction: row;
`;

const Presentation = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
`;

const AnimatedStates = styled.div`
	position: relative;
	width: 100%;
	height: 300px;
`;

const AnimateActions = styled.div`
	position: relative;
	width: 100%;
	height: 300px;
	display: flex;
	flex-direction: column;
	align-items: center;
`;

class App extends React.Component {
	constructor(...args) {
		super(...args);
		this.state = {
			top: 100,
			left: 100,
			angle: 0,
			scale: 2,
			runActions: false,
		};
	}

	shiftUp = () => this.setState({ top: this.state.top - 50 });
	shiftDown = () => this.setState({ top: this.state.top + 50 });
	shiftLeft = () => this.setState({ left: this.state.left - 50 });
	shiftRight = () => this.setState({ left: this.state.left + 50 });
	cw = () => this.setState({ angle: this.state.angle + 30 });
	ccw = () => this.setState({ angle: this.state.angle - 30 });
	zoomIn = () => this.setState({ scale: this.state.scale * 2 });
	zoomOut = () => this.setState({ scale: this.state.scale / 2 });
	toggleAnimateActions = () => this.setState({ runActions: !this.state.runActions });

	render() {
		return (
			<WholeScreen>
				<ButtonArea>
					<button onClick={this.shiftLeft}>←</button>
					<button onClick={this.shiftUp}>↑</button>
					<button onClick={this.shiftDown}>↓</button>
					<button onClick={this.shiftRight}>→</button>
					<button onClick={this.cw}>↻</button>
					<button onClick={this.ccw}>↺</button>
					<button onClick={this.zoomIn}>+</button>
					<button onClick={this.zoomOut}>-</button>
				</ButtonArea>

				<Presentation>
					<AnimatedStates>
						<AnimatedPosition duration={2000} top={this.state.top} left={this.state.left}>
							<AnimatedScale duration={2000} scaleX={this.state.scale} scaleY={this.state.scale}>
								<AnimatedRotation duration={2000} angleZ={this.state.angle}>
									<div>What is This?</div>
								</AnimatedRotation>
							</AnimatedScale>
						</AnimatedPosition>
					</AnimatedStates>
					<AnimateActions>
						<button onClick={this.toggleAnimateActions}>{this.state.runActions ? 'Unmount' : 'Run'}</button>
						{this.state.runActions && (
							<AnimateMove duration={2000} start={{ top: 50, left: 30 }} end={{ top: this.state.top, left: this.state.left }}>
								<AnimateScale duration={2000} start={{ X: 1, Y: 1}} end={{ X: this.state.scale, Y: this.state.scale}}>
									<AnimateRotate duration={2000} start={{ Z: 10 }} end={{ Z: this.state.angle }}>
										<div>Who are You?</div>
									</AnimateRotate>
								</AnimateScale>
							</AnimateMove>
						)}
					</AnimateActions>
				</Presentation>
			</WholeScreen>
		);
	}
}

export default App;