const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
	mode: 'development',
	entry: ['./src/DeclareAnimate.js'],
	output: {
		filename: 'web-bundle.js',
		path: path.resolve(__dirname, 'dist'),
		library: 'ReactDeclareAnimateWeb',
        libraryTarget: 'umd',
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/env', '@babel/react'],
						plugins: ['transform-object-rest-spread', 'transform-class-properties'],
					},
				},
			},
		],
	},
	resolve: {
		extensions: ['.jsx', '.js'],
	},
};
