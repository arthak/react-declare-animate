module.exports = {
	mode: 'development',
	entry: ['./src/native/DeclareAnimate.js'],
	externals: {
		'react-native': 'react-native'
	},
	output: {
		filename: 'native.js',
		path: __dirname,
		library: 'ReactDeclareAnimateNative',
        libraryTarget: 'umd'
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/env', '@babel/react'],
						plugins: ['transform-object-rest-spread', 'transform-class-properties'],
					},
				},
			},
		],
	},
	resolve: {
		extensions: ['.jsx', '.js'],
	},
};
