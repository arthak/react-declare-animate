# react-declare-animate
A set of declarative components for animating your objects

<a name='Description'></a>
<h1>Description</h1>
<p>
    This is a set of React components which provide the programmer with a way to create simple animations in a declarative manner. It works for both web and React Native. Every animation component wraps its children with a DIV for web, or View for React Native, which can be additionally styled by passing a <tt>style</tt> parameter.
</p>

<p>A simple example:</p>

<pre>
    &lt;AnimatedPosition top={this.state.top} left={this.state.left} duration={2000} &gt;
        &lt;img src="..." /&gt;
    &lt;/AnimatedPosition&gt;
</pre>

<p>
    Library size when installed: <b>32K</b> for Web, <b>44K</b> for React Native.
</p>

<h1>Table of Contents</h1>
<ul>
    <li><a href='#Description'>Description</a></li>
    <li><a href='#Installation'>Installation</a></li>
    <li><a href='#Details'>Details</a>
        <ul>
            <li><a href='#AnimatedStates'>Animated States</a>
                <ul>
                    <li><a href='#AnimatedPosition'>AnimatedPosition</a></li>
                    <li><a href='#AnimatedRotation'>AnimatedRotation</a></li>
                    <li><a href='#AnimatedScale'>AnimatedScale</a></li>
                    <li><a href='#AnimatedSkew'>AnimatedSkew</a></li>
                    <li><a href='#AnimatedPerspective'>AnimatedPerspective</a></li>
                </ul>
            </li>
            <li><a href='#AnimationActions'>Animation Actions</a>
                <ul>
                    <li><a href='#AnimateMove'>AnimateMove</a></li>
                    <li><a href='#AnimateRotate'>AnimateRotate</a></li>
                    <li><a href='#AnimateScale'>AnimateScale</a></li>
                    <li><a href='#AnimateSkew'>AnimateSkew</a></li>
                    <li><a href='#AnimatePerspective'>AnimatePerspective</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li><a href='#CommonProperties'>Common Properties</a></li>
    <li><a href='#ExampleProjects'>Example Projects</a></li>
    <li><a href='#BugsAndHacks'>Bugs & Hacks</a></li>
	<li><a href='#Projects'>Organizations and projects using react-radio-button-group</a></li>
</ul>

<a name='Installation'></a>
<h1>Installation</h1>
<pre>
    yarn add react-declare-animate
</pre>

<a name='Details'></a>
<h1>Details</h1>
<p>
    There are two types of animations: <i><a href='#AnimationActions'>animation actions</a></i> and <i><a href='#AnimatedStates'>animated states</a></i>.
</p>
<a name='AnimatedStates'></a>
<h2>Animated states</h2>
<p>
    <b>An animated state</b> is a declaration of a child object's props which animate when they are changed. E.g. if you wrap an image in <tt>&lt;AnimatedPosition top={10} &gt;</tt>, and then change top=10 to top=50, the image will make an animated transition from top=10 to top=50.
</p>
<img width="350px" src="https://gitlab.com/dmaksimovic/react-declare-animate/raw/master/animated-position.gif" />
<a name='AnimatedPosition'></a>
<h3>AnimatedPosition</h3>
<pre>
    &lt;AnimatedPosition
        ...
        top={currentTop} [in pixels]
        left={currentLeft} [in pixels]
    &gt;
</pre>
<p>
    Sets the child at the given coordinates, and when they move, an animated transition will happen between the old and the new value.
</p>
<a name='AnimatedRotation'></a>
<h3>AnimatedRotation</h3>
<pre>
    &lt;AnimatedRotation
        ...
        angleX={...} [all angles in radians. One of more axes may be specified]
        angleY={...}
        angleZ={...}
    &gt;
</pre>
<p>
    Displays the child rotated by the given angles around the given axis. When any of the angle props change, an animated transition will occur.
</p>
<a name='AnimatedScale'></a>
<h3>AnimatedScale</h3>
<pre>
    &lt;AnimatedScale
        ...
        scaleX={...} [a number by which the X-dimension is multiplied]
        scaleY={...} [a number by which the Y-dimension is multiplied]
    &gt;
</pre>
<p>
    When scaleX (or scaleY, or both) are changed, they will not only snap, but animate instead.
</p>
<a name='AnimatedSkew'></a>
<h3>AnimatedSkew</h3>
<pre>
    &lt;AnimatedSkew
        ...
        skewX={...} [a skew angle in radians]
        skewX={...} [a skew angle in radians]
    &gt;
</pre>
<a name='AnimatedPerspective'></a>
<h3>AnimatedPerspective</h3>
<pre>
    &lt;AnimatedPerspective
        ...
        perspective={...} [the perspective point distance in pixels]
    &gt;
</pre>
<a name='AnimationActions'></a>
<h2>Animation actions</h2>
<p>
    <b>The animation actions</b> have <tt>start</tt> and <tt>end</tt> parameters. They start animating immediatelly on mount, execute for the given time and when they finish, they can only be restarted by remounting. Once instantiated, they will run on first mount to the end, and they cannot be affected by changing props at any moment. You can pass <tt>onFinished</tt> callback, to be notified when the animation is finished.
</p>
<p>
    E.g.:
</p>
<pre>
    &lt;AnimateMove
        onFinished={() => {}}
        start={{
            top: 10,
            left: 10,
        }}
        end={{
            top: 50,
            left: 50,
        }}
        duration={2000}
    &gt;
        &lt;img src="..." /&gt;
    &lt;/AnimateMove&gt;
</pre>    

<p>
    The image will start moving on mount, and will move from (10, 10) to (50, 50) within 2 seconds. Once it arrives to (50, 50), it will stay there, regardless of any prop change. The only way to restart an animation action is to remount it.
</p>

<a name='AnimateMove'></a>
<h3>AnimateMove</h3>
<pre>
    &lt;AnimateMove
        ...
        start={{ top, left }} [start and end positions, in pixels]
        end={{ top, left }} [in pixels, required]
    &gt;
        {child}
    &lt;AnimateMove /&gt;
</pre>
<p>
    Moves the child from <tt>start</tt> to <tt>end</tt> position.
</p>

<a name='AnimateRotate'></a>
<h3>AnimateRotate</h3>
<pre>
    &lt;AnimateRotate
        ...
        start={{ X, Y, Z }} [start and end X, Y and/or Z angles in radians]
        end={{ X, Y, Z }}
    &gt;
</pre>
<p>
    Rotates the child over the given axis (X, Y, Z or some combinations, depending on which of them you define), by the given angle in radians.
</p>
<p>
    Example:
</p>
<pre>
    &lt;AnimateRotate
        duration={2000}
        start={{ Z: 30 }}
        end={{ Z: 80 }}
    &gt;
        &lt;span&gt;I'm rotating&lt;/span&gt;
    &lt;/AnimateRotate&gt;
</pre>
<p>
    This piece of code, when mounted, will show the SPAN rotated at 30&deg; around the Z-axis as the first frame, and immediately start rotating it until it reaches 80&deg;, 2 seconds later.
</p>
<a name='AnimateScale'></a>
<h3>AnimateScale</h3>
<pre>
    &lt;AnimateScale
        ...
        start={{ X, Y }} [start and end, X and Y multiplication factors]
        end={{ X, Y }}
    &gt;
</pre>
<p>
    Scales the child over a given time period by the given X and/or Y axis.
</p>
<a name='AnimateSkew'></a>
<h3>AnimateSkew</h3>
<pre>
    &lt;AnimateSkew
        ...
        start={{ X, Y }} [start and end angles in degrees; one or both may be specified]
        end={{ X, Y }}
    &gt;
</pre>
<a name='AnimatePerspective'></a>
<h3>AnimatePerspective</h3>
<pre>
    &lt;AnimatePerspective
        start={startPerspective} [number]
        end={endPerspective} [number]
    &gt;
</pre>
<a name='CommonProperties'></a>
<h1>Common properties</h1>
<p>All animation objects can also receive these props:</p>
<ul>
    <li><b><tt>duration</tt></b> - every animation takes a duration parameter, which specifies the time needed for that animation to finish. For animated state objects, if some props are changed while an existing animation is in progress, then that existing animation will abort and a new animation with full duration will start immediately, with the current state as the starting point.</li>
    <li><b><tt>style</tt></b> - an object with additional CSS to apply on the enwrapping DIV / View (good examples are custom z-index, unwanted margins, paddings etc). The default is an empty object.</li>
    <li><b><tt>getEasingFunc</tt></b> - a function returning the easing/timing function to use in the animation. The expected result of the function must conform to the current platform (web / React Native), so it is up to the developer to make sure this custom function returns what is expected at the current platform. Easing functions for React Native are described <a href="https://facebook.github.io/react-native/docs/easing.html">here</a>, and easing functions for web are described <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/transition-timing-function">here.</a> The default is <tt>'ease'</tt> for web, and <tt>Easing.in()</tt> for React Native.</li>
    <li><b><tt>useNativeDriver</tt></b> - React Native specific (does not do anything for web), specifies whether or not to use this flag in animations. For React Native, this flag will help provide faster animations, by utilizing some system-specific animation threads. The default is false.</li>
</ul>

<a name='ExampleProjects'></a>
<h1>Example projects</h1>
<p>Two examples projects are provided, one for web, and one for React Native. Both are contained in the GitLab repo of this library, at <a href="https://gitlab.com/dmaksimovic/react-declare-animate">https://gitlab.com/dmaksimovic/react-declare-animate</a>, in the <tt>example</tt> directory.</p>
<ul>
<li><a href='https://gitlab.com/dmaksimovic/react-declare-animate/tree/master/example/native'>React Native test project</a></li>
<li><a href='https://gitlab.com/dmaksimovic/react-declare-animate/tree/master/example/web'>Web test project</a></li>
</ul

<a name="BugsAndHacks"></a>
<h1>Bugs & Hacks</h1>
<ul>
<li>React Native has an awkward behavior regarding Views which don't have a fixed width and height. Even if its content is empty, a View will always have some random width and height if they are not explicitly set. Since all animation objects of this library wrap their children in a View, it is often needed to set width and height manually by using the style={...} override. The library cannot fix this alone, because there is no clear way provided by React Native to ask a View to "fit to content".</li>
<li>It is often needed to set a transformation origin point (transform-origin) when doing geometrical transformations. CSS provides a clear way to do this (by using the <tt>transform-origin</tt> directive), but React Native does not provide this, therefore the library does not provide it all, otherwise it would be inconsistent across Web &lt;-&gt; Native. There is a short discussion <a href="https://stackoverflow.com/questions/47157408/animating-transform-origin-in-react-native">on StackOverflow</a> about this problem, and there was a request for adding transform-origin to React Native <a href="https://github.com/facebook/react-native/issues/1964">on GitHub here</a>.</li>
<li>There is a known bug in React Native that scale[XY] does not work on Android when set to 0 (zero) as the initial value. It behaves as if it was set to 1 instead. When you start animating however, it behaves as if it was indeed set to zero. There has been a bug opened on <a href="https://github.com/facebook/react-native/issues/6278">RN's GitHub issues page here</a>, but it was closed due to inactivity, and a new one was opened on <a href="https://github.com/facebook/react-native/issues/16677">another RN's GitHub issues page here</a>, but that one was closed unfixed as well. The general hack/workaround is to use 0.01 or similar, instead of just zero (note though, for some reason many zeroes like 0.00001 also don't work). Instead of introducing this hack to react-declare-animate and making it dirty, it is suggested to users to use 0.01 etc. with AnimateScale and AnimatedScale, instead of 0, until guys from React Native fix their bug.</li>
</ul>

<a name='Projects'></a>
<h1>Organizations and projects using react-radio-button-group</h1>

[<img src="https://mauking.com/www-assets/get-social/fb-logo.png" width="100px;"/><br /><sub><b>Mau King - Mau Mau, Crazy 8s, the card game</b></sub>](https://mauking.com)<br />