const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
	mode: 'production',
	entry: ['./src/native/DeclareAnimate.js'],
	externals: {
		'react-native': 'react-native'
	},
	output: {
		filename: 'native.js',
		path: __dirname,
		library: 'ReactDeclareAnimateNative',
        libraryTarget: 'umd'
	},
	optimization: {
		minimizer: [
			new UglifyJsPlugin({
				cache: true,
				parallel: true,
				uglifyOptions: {
					compress: false,
					ecma: 6,
					mangle: true,
				},
			}),
		],
	},
	plugins: [
		new webpack.optimize.AggressiveMergingPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('production'),
		}),
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/env', '@babel/react'],
						plugins: ['transform-object-rest-spread', 'transform-class-properties'],
					},
				},
			},
		],
	},
	resolve: {
		extensions: ['.jsx', '.js'],
	},
};
