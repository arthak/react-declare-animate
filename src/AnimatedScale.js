import React from 'react';
import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

const calculateTransformString = (scaleX, scaleY) => {
	const supportedTransformations = [
		{ value: scaleX, cssName: 'scaleX' },
		{ value: scaleY, cssName: 'scaleY' },
	];
	const scaleDescriptors = supportedTransformations.map(({ value, cssName }) =>
		isDefined(value) ? `${cssName}(${value})` : null,
	);
	const validScaleDescriptors = scaleDescriptors.filter(rd => rd !== null);
	const transformString = validScaleDescriptors.join(' ');
	if (!transformString) {
		return {};
	}
	return {
		transform: transformString,
	};
};

const AnimatedScale = ({ scaleX, scaleY, duration, children, getEasingFunc, style = {}, ...customProps }) => (
	<div
		style={{
			...calculateTransformString(scaleX, scaleY),
			transition: `transform ${duration / 1000}s`,
			transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
			...style,
		}}
		{...customProps}
	>
		{children}
	</div>
);

AnimatedScale.displayName = 'AnimatedScale';

AnimatedScale.propTypes = {
	...GeneralAnimationPropTypes,
	scaleX: PropTypes.number,
	scaleY: PropTypes.number,
};

export default AnimatedScale;
