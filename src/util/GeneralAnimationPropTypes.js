import PropTypes from 'prop-types';

export default {
	duration: PropTypes.number,
	getEasingFunc: PropTypes.func,
	useNativeDriver: PropTypes.bool,
	children: PropTypes.node,
	style: PropTypes.object, // eslint-disable-line react/forbid-prop-types
	renderToHardwareTextureAndroid: PropTypes.bool,
};