const isProduction = process.env.NODE_ENV === 'production';
const isDevelopment = !isProduction;

export default (className, logId, message) => 
	isDevelopment && console.log(`${new Date().getTime()}: ${className}[id=${logId}].${message}`);

export const getLogId = () => isDevelopment ? Math.round(Math.random() * 10000) : null;