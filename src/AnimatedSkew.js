import React from 'react';
import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

const calculateTransformString = (skewX, skewY) => {
	const supportedTransformations = [
		{ value: skewX, cssName: 'skewX' },
		{ value: skewY, cssName: 'skewY' },
	];
	const skewDescriptors = supportedTransformations.map(({ value, cssName }) =>
		isDefined(value) ? `${cssName}(${value}rad)` : null,
	);
	const validSkewDescriptors = skewDescriptors.filter(rd => rd !== null);
	const transformString = validSkewDescriptors.join(' ');
	if (!transformString) {
		return {};
	}
	return {
		transform: transformString,
	};
};

const AnimatedSkew = ({ skewX, skewY, duration, children, getEasingFunc, style = {}, ...customProps }) => (
	<div
		style={{
			...calculateTransformString(skewX, skewY),
			transition: `transform ${duration / 1000}s`,
			transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
			...style,
		}}
		{...customProps}
	>
		{children}
	</div>
);

AnimatedSkew.displayName = 'AnimatedSkew';

AnimatedSkew.propTypes = {
	...GeneralAnimationPropTypes,
	skewX: PropTypes.number,
	skewY: PropTypes.number,
};

export default AnimatedSkew;
