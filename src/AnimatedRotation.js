import React from 'react';
import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

const calculateTransformString = (angleX, angleY, angleZ) => {
	const supportedTransformations = [
		{ value: angleX, cssName: 'rotateX' },
		{ value: angleY, cssName: 'rotateY' },
		{ value: angleZ, cssName: 'rotateZ' },
	];
	const rotationDescriptors = supportedTransformations.map(({ value, cssName }) =>
		isDefined(value) ? `${cssName}(${value}rad)` : null,
	);
	const validRotationDescriptors = rotationDescriptors.filter(rd => rd !== null);
	const transformString = validRotationDescriptors.join(' ');
	if (!transformString) {
		return {};
	}
	return {
		transform: transformString,
	};
};

const AnimatedRotation = ({
	angleX,
	angleY,
	angleZ,
	duration,
	children,
	getEasingFunc,
	style = {},
	...customProps
}) => (
	<div
		style={{
			...calculateTransformString(angleX, angleY, angleZ),
			transition: `transform ${duration / 1000}s`,
			transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
			...style,
		}}
		{...customProps}
	>
		{children}
	</div>
);

AnimatedRotation.displayName = 'AnimatedRotation';

AnimatedRotation.propTypes = {
	...GeneralAnimationPropTypes,
	angleX: PropTypes.number,
	angleY: PropTypes.number,
	angleZ: PropTypes.number,
};

export default AnimatedRotation;
