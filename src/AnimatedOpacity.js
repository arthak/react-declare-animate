import React from 'react';
import PropTypes from 'prop-types';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

const AnimatedOpacity = ({ opacity, duration, children, getEasingFunc, style = {}, ...customProps }) => (
	<div
		style={{
			opacity,
			transition: `opacity ${duration / 1000}s`,
			transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
			...style,
		}}
		{...customProps}
	>
		{children}
	</div>
);

AnimatedOpacity.displayName = 'AnimatedOpacity';

AnimatedOpacity.propTypes = {
	...GeneralAnimationPropTypes,
	opacity: PropTypes.number,
};

export default AnimatedOpacity;
