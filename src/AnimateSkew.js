import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';
import AnimateAction from './AnimateAction';

class AnimateSkew extends AnimateAction {
	constructor(...args) {
		super(['transform'], ...args);
	}

	getStyle(skewFactor) {
		const { X, Y } = skewFactor;
		const transformations = 
			[
				['skewX', X],
				['skewY', Y],
			]
			.reduce(
				(transformations, [transformName, value]) =>
					isDefined(value) ? `${transformations} ${transformName}(${value}rad)` : transformations,
				'',
			);
		return {
			transform: transformations || 'none',
		};
	}
}

AnimateSkew.displayName = 'AnimateSkew';

AnimateSkew.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
	}),
	end: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
	}),
};

export default AnimateSkew;
