import React from 'react';
import PropTypes from 'prop-types';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

const AnimatedTransform = ({
	left,
	top,
	duration,
	children,
	getEasingFunc,
	style = {},
	width,
	height,
	opacity,
	...customProps
}) => (
	<div
		style={{
			position: 'absolute',
			top,
			left,
			width,
			height,
			opacity,
			transition: `top ${duration / 1000}s, left ${duration / 1000}s`,
			transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
			...style,
		}}
		{...customProps}
	>
		{children}
	</div>
);

AnimatedTransform.displayName = 'AnimatedTransform';

AnimatedTransform.propTypes = {
	...GeneralAnimationPropTypes,
	top: PropTypes.number,
	left: PropTypes.number,
};

export default AnimatedTransform;
