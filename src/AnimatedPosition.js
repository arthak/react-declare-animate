import React from 'react';
import PropTypes from 'prop-types';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

const AnimatedPosition = ({ left, top, duration, children, getEasingFunc, style = {}, ...customProps }) => (
	<div
		style={{
			position: 'absolute',
			top,
			left,
			transition: `top ${duration / 1000}s, left ${duration / 1000}s`,
			transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
			...style,
		}}
		{...customProps}
	>
		{children}
	</div>
);

AnimatedPosition.displayName = 'AnimatedPosition';

AnimatedPosition.propTypes = {
	...GeneralAnimationPropTypes,
	top: PropTypes.number,
	left: PropTypes.number,
};

export default AnimatedPosition;
