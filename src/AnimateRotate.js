import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';
import AnimateAction from './AnimateAction';

class AnimateRotate extends AnimateAction {
	constructor(...args) {
		super(['transform'], ...args);
	}

	getStyle(angle) {
		const { X, Y, Z } = angle;
		const transformations = 
			[
				['rotateX', X],
				['rotateY', Y],
				['rotateZ', Z],
			]
			.reduce(
				(transformations, [transformName, value]) =>
					isDefined(value) ? `${transformations} ${transformName}(${value}rad)` : transformations,
				'',
			);

		return {
			transform: transformations || 'none',
		};
	}
}

AnimateRotate.displayName = 'AnimateRotate';

AnimateRotate.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
		Z: PropTypes.number,
	}),
	end: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
		Z: PropTypes.number,
	}),
};

export default AnimateRotate;
