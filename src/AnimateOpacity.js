import PropTypes from 'prop-types';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';
import AnimateAction from './AnimateAction';

class AnimateOpacity extends AnimateAction {
	constructor(...args) {
		super(['opacity'], ...args);
	}

	getStyle(opacity) {
		return {
			opacity,
		};
	}
}

AnimateOpacity.displayName = 'AnimateOpacity';

AnimateOpacity.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.number,
	end: PropTypes.number,
};

export default AnimateOpacity;
