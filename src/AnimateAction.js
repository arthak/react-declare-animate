/* global window */
import React from 'react';
import PropTypes from 'prop-types';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

class AnimateAction extends React.Component {
	constructor(transitions, ...args) {
		super(...args);
		this.transitions = transitions;
		this.state = {};
	}

	componentDidMount() {
		const { end } = this.props;
		window.requestAnimationFrame(
			() => setTimeout(
				() => this.setState(
					{
						end,
					},
					this.scheduleEndCallback,
				),
				0,
			),
		);
	}

	scheduleEndCallback = () => {
		const { onFinished, duration } = this.props;
		if (!onFinished) {
			return false;
		}
		setTimeout(onFinished, duration);
	};

	render() {
		const { start, duration, style, getEasingFunc, children } = this.props;
		const { end } = this.state;
		return (
			<div
				style={{
					transition: this.transitions.map(t => `${t} ${duration / 1000}s`).join(','),
					transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
					...this.getStyle(end || start),
					...style,
				}}
			>
				{children}
			</div>
		);
	}
}

AnimateAction.propTypes = {
	...GeneralAnimationPropTypes,
	onFinished: PropTypes.func,
};

export default AnimateAction;
