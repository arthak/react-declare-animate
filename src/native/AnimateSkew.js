import PropTypes from 'prop-types';
import AnimateAction from './AnimateAction';
import { calculateMiddleValue } from './arithmetics';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import isDefined from '../util/isDefined';

class AnimateSkew extends AnimateAction {
	constructor(...args) {
		super(...args);
		this.className = 'AnimateSkew';
	}

	getAnimatedStyle(start, end, animationValue) {
		const {
			X: startX = 0,
			Y: startY = 0,
		} = start;
		const {
			X: endX,
			Y: endY,
		} = end;

		const transformations = [];
		if (isDefined(endX)) {
			transformations.push({
				skewX: calculateMiddleValue(startX, endX, animationValue),
			});
		}

		if (isDefined(endY)) {
			transformations.push({
				skewY: calculateMiddleValue(startY, endY, animationValue),
			});
		}

		const style = {
			transform: transformations,
		};
		
		return style;
	}
}

AnimateSkew.displayName = 'AnimateSkew';

AnimateSkew.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
	}),
	end: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
	}),
};

export default AnimateSkew;
