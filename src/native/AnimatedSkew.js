import PropTypes from 'prop-types';
import AnimatedProperties from './AnimatedProperties';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';

class AnimatedSkew extends AnimatedProperties {
	constructor(props) {
		super('AnimatedSkew', props, () => [
			{ propName: 'skewX', transformationName: 'skewX' },
			{ propName: 'skewY', transformationName: 'skewY' },
		]);
	}
}

AnimatedSkew.displayName = 'AnimatedSkew';

AnimatedSkew.propTypes = {
	...GeneralAnimationPropTypes,
	skewX: PropTypes.number,
	skewY: PropTypes.number,
};

export default AnimatedSkew;
