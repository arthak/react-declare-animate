import React from 'react';
import { Animated, Easing } from 'react-native';
import isDefined from '../util/isDefined';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import log, { getLogId } from '../util/log';

const INITIAL_STATE = {
	animationValue: undefined,
	startValues: {},
	endValues: {},
	staticValues: {},
};

class AnimatedProperties extends React.Component {
	constructor(className, props, getProperties, position) {
		super(props);
		this.className = className;
		this.logId = getLogId();

		this.getProperties = getProperties;
		this.state = INITIAL_STATE;
		this.position = position;
		this.currentValues = {};
	}

	componentDidMount() {
		const staticValues = this.getStaticValuesFromProps();

		this.setState({
			staticValues,
		});
	}

	componentDidUpdate(previousProps) {
		const isAnyValueChanged = this.getProperties().find(
			({ propName }) => previousProps[propName] !== this.props[propName],
		);
		if (isAnyValueChanged) {
			this.animateProperties(previousProps);
		}
	}

	getStaticValuesFromProps = () =>
		this.getProperties().reduce(
			(staticValues, { propName }) => ({
				...staticValues,
				[propName]: this.props[propName],
			}),
			{},
		);

	logText = str => log(this.className, this.logId, str);

	recordCurrentValues = ({ value: currentAnimationValue }) => {
		this.currentAnimationValue = currentAnimationValue;
	};

	produceCurrentValues = currentAnimationValue =>
		this.getProperties().reduce((currentValues, { propName }) => {
			const startValue = this.state.startValues[propName];
			const endValue = this.state.endValues[propName];
			if (!isDefined(startValue) || !isDefined(endValue)) {
				return currentValues;
			}
			return {
				...currentValues,
				[propName]: startValue + currentAnimationValue * (endValue - startValue),
			};
		}, {});

	resetToStaticPosition = ({ finished }) => {
		delete this.animation;
		delete this.currentAnimationValue;
		const staticValues = this.getStaticValuesFromProps();

		const newState = {
			...INITIAL_STATE,
			staticValues,
		};
		this.setState(newState);
	};

	animateProperties = previousProps => {
		const { duration, useNativeDriver } = this.props;

		const newAnimationValue = new Animated.Value(0);
		newAnimationValue.addListener(this.recordCurrentValues);

		const startValues = {};
		const endValues = {};

		const currentValues = this.produceCurrentValues(this.currentAnimationValue);
		this.getProperties().forEach(({ propName }) => {
			const previousValue = previousProps[propName];
			const newValue = this.props[propName];
			const currentValue = currentValues[propName];

			if (newValue !== (currentValue || previousValue)) {
				startValues[propName] = currentValue || previousValue;
				endValues[propName] = newValue;
			}
		});

		const { animation: existingAnimation } = this;
		if (existingAnimation) {
			existingAnimation.stop();
		}

		this.setState(
			{
				animationValue: newAnimationValue,
				startValues,
				endValues,
				fixedValues: {},
			},
			() => {
				this.animation = Animated.timing(newAnimationValue, {
					toValue: 1,
					duration,
					easing: this.props.getEasingFunc ? this.props.getEasingFunc() : Easing.in(),
					useNativeDriver,
				});
				const animationFinishedHandler = this.resetToStaticPosition;
				this.animation.start(animationFinishedHandler);
			},
		);
	};

	render() {
		const { children, style = {}, ...customProps } = this.props;
		const { animationValue, startValues, endValues, staticValues } = this.state;

		const transformations = [];
		const dynamicStyles = {};
		let animated = false;

		this.getProperties().forEach(({ propName, transformationName, dynamicCssName }) => {
			let propertyValue = undefined;
			if (isDefined(endValues[propName])) {
				propertyValue = animationValue.interpolate({
					inputRange: [0, 1],
					outputRange: [startValues[propName], endValues[propName]],
				});
				animated = true;
			} else if (isDefined(staticValues[propName])) {
				propertyValue = staticValues[propName];
			}

			if (isDefined(propertyValue)) {
				if (transformationName) {
					transformations.push({
						[transformationName]: propertyValue,
					});
				} else if (dynamicCssName) {
					dynamicStyles[dynamicCssName] = propertyValue;
				}
			}
		});

		return (
			<Animated.View
				renderToHardwareTextureAndroid={this.props.renderToHardwareTextureAndroid && animated}
				style={{
					position: this.position,
					transform: transformations,
					...dynamicStyles,
					...style,
				}}
				{...customProps}
			>
				{children}
			</Animated.View>
		);
	}
}

AnimatedProperties.propTypes = GeneralAnimationPropTypes;

export default AnimatedProperties;
