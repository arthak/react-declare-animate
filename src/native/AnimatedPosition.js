import PropTypes from 'prop-types';
import AnimatedProperties from './AnimatedProperties';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';

class AnimatedPosition extends AnimatedProperties {
	constructor(props) {
		super('AnimatedPosition', props, () => [
			{ propName: 'top', transformationName: 'translateY' },
			{ propName: 'left', transformationName: 'translateX' },
		], 'absolute');
	}
}

AnimatedPosition.displayName = 'AnimatedPosition';

AnimatedPosition.propTypes = {
	...GeneralAnimationPropTypes,
	top: PropTypes.number,
	left: PropTypes.number,
};

export default AnimatedPosition;
