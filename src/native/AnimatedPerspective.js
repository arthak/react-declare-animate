import PropTypes from 'prop-types';
import AnimatedProperties from './AnimatedProperties';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';

class AnimatedPerspective extends AnimatedProperties {
	constructor(props) {
		super('AnimatedPerspective', props, () => [
			{ propName: 'perspective', transformationName: 'perspective' },
		]);
	}
}

AnimatedPerspective.displayName = 'AnimatedPerspective';

AnimatedPerspective.propTypes = {
	...GeneralAnimationPropTypes,
	perspective: PropTypes.number,
};

export default AnimatedPerspective;
