import { Animated } from 'react-native';

// eslint-disable-next-line
export const calculateMiddleValue = (start, end, animationValue) =>
	Animated.add(start, Animated.multiply(animationValue, end - start));
