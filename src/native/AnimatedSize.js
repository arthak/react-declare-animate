import PropTypes from 'prop-types';
import AnimatedProperties from './AnimatedProperties';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';

class AnimatedSize extends AnimatedProperties {
	constructor(props) {
		super('AnimatedSize', props, () => [
			{ propName: 'width', dynamicCssName: 'width' },
			{ propName: 'height', dynamicCssName: 'height' },
		]);
	}
}

AnimatedSize.displayName = 'AnimatedSize';

AnimatedSize.propTypes = {
	...GeneralAnimationPropTypes,
	width: PropTypes.number,
	height: PropTypes.number,
};

export default AnimatedSize;
