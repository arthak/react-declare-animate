import PropTypes from 'prop-types';
import AnimateAction from './AnimateAction';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import { calculateMiddleValue } from './arithmetics';
import isDefined from '../util/isDefined';

class AnimatePerspective extends AnimateAction {
	constructor(...args) {
		super(...args);
		this.className = 'AnimatePerspective';
	}

	getAnimatedStyle(start, end, animationValue) {
		const transformations = [];
		if (isDefined(end)) {
			transformations.push({
				perspective: calculateMiddleValue(start, end, animationValue),
			});
		}

		const style = {
			transform: transformations,
		};
		
		return style;
	}
}

AnimatePerspective.displayName = 'AnimatePerspective';

AnimatePerspective.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.number,
	end: PropTypes.number,
};

export default AnimatePerspective;
