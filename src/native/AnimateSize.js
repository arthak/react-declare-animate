import PropTypes from 'prop-types';
import AnimateAction from './AnimateAction';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import { calculateMiddleValue } from './arithmetics';
import isDefined from '../util/isDefined';

class AnimateSize extends AnimateAction {
	constructor(...args) {
		super(...args);
		this.className = 'AnimateSize';
	}

	getAnimatedStyle(start, end, animationValue) {
		const {
			width: startWidth = 0,
			height: startHeight = 0,
		} = start;
		const {
			width: endWidth,
			height: endHeight,
		} = end;
		const style = {};

		if (isDefined(endWidth)) {
			style.width = calculateMiddleValue(startWidth, endWidth, animationValue);
		}
		if (isDefined(endHeight)) {
			style.height = calculateMiddleValue(startHeight, endHeight, animationValue);
		}
		
		return style;
	}
}

AnimateSize.displayName = 'AnimateSize';

AnimateSize.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		width: PropTypes.number,
		height: PropTypes.number,
	}),
	end: PropTypes.shape({
		width: PropTypes.number,
		height: PropTypes.number,
	}),
};

export default AnimateSize;
