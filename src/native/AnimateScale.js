import PropTypes from 'prop-types';
import AnimateAction from './AnimateAction';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import { calculateMiddleValue } from './arithmetics';
import isDefined from '../util/isDefined';

class AnimateScale extends AnimateAction {
	constructor(...args) {
		super(...args);
		this.className = 'AnimateScale';
	}

	getAnimatedStyle(start, end, animationValue) {
		const {
			X: startX = 0,
			Y: startY = 0,
		} = start;
		const {
			X: endX,
			Y: endY,
		} = end;

		const transformations = [];
		if (isDefined(endX)) {
			transformations.push({
				scaleX: calculateMiddleValue(startX, endX, animationValue),
			});
		}

		if (isDefined(endY)) {
			transformations.push({
				scaleY: calculateMiddleValue(startY, endY, animationValue),
			});
		}

		const style = {
			transform: transformations,
		};
		
		return style;
	}
}

AnimateScale.displayName = 'AnimateScale';

AnimateScale.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
	}),
	end: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
	}),
};

export default AnimateScale;
