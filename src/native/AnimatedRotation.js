import PropTypes from 'prop-types';
import AnimatedProperties from './AnimatedProperties';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';

class AnimatedRotation extends AnimatedProperties {
	constructor(props) {
		super('AnimatedRotation', props, () => [
			{ propName: 'angleX', transformationName: 'rotateX' },
			{ propName: 'angleY', transformationName: 'rotateY' },
			{ propName: 'angleZ', transformationName: 'rotateZ' },
		]);
	}
}

AnimatedRotation.displayName = 'AnimatedRotation';

AnimatedRotation.propTypes = {
	...GeneralAnimationPropTypes,
	angleX: PropTypes.number,
	angleY: PropTypes.number,
	angleZ: PropTypes.number,
};

export default AnimatedRotation;
