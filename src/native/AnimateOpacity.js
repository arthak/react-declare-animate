import PropTypes from 'prop-types';
import AnimateAction from './AnimateAction';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import { calculateMiddleValue } from './arithmetics';
import isDefined from '../util/isDefined';

class AnimateOpacity extends AnimateAction {
	constructor(...args) {
		super(...args);
		this.className = 'AnimateOpacity';
	}

	getAnimatedStyle(startOpacity, endOpacity, animationValue) {
		if (!isDefined(startOpacity)) {
			return {};
		}

		if (!isDefined(endOpacity)) {
			return {
				opacity: startOpacity,
			};
		}

		const style = {
			opacity: calculateMiddleValue(startOpacity, endOpacity, animationValue),
		};
		
		return style;
	}
}

AnimateOpacity.displayName = 'AnimateOpacity';

AnimateOpacity.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.number,
	end: PropTypes.number,
};

export default AnimateOpacity;
