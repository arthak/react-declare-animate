import PropTypes from 'prop-types';
import AnimatedProperties from './AnimatedProperties';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';

class AnimatedOpacity extends AnimatedProperties {
	constructor(props) {
		super('AnimatedOpacity', props, () => [
			{ propName: 'opacity', dynamicCssName: 'opacity' },
		]);
	}
}

AnimatedOpacity.displayName = 'AnimatedOpacity';

AnimatedOpacity.propTypes = {
	...GeneralAnimationPropTypes,
	opacity: PropTypes.number,
};

export default AnimatedOpacity;
