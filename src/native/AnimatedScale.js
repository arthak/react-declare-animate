import PropTypes from 'prop-types';
import AnimatedProperties from './AnimatedProperties';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';

class AnimatedScale extends AnimatedProperties {
	constructor(props) {
		super('AnimatedScale', props, () => [
			{ propName: 'scaleX', transformationName: 'scaleX' },
			{ propName: 'scaleY', transformationName: 'scaleY' },
		]);
	}
}

AnimatedScale.displayName = 'AnimatedScale';

AnimatedScale.propTypes = {
	...GeneralAnimationPropTypes,
	scaleX: PropTypes.number,
	scaleY: PropTypes.number,
};

export default AnimatedScale;
