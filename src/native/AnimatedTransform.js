import PropTypes from 'prop-types';
import AnimatedProperties from './AnimatedProperties';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';

class AnimatedTransform extends AnimatedProperties {
	constructor(props) {
		super('AnimatedTransform', props, () => [
			{ propName: 'width', dynamicCssName: 'width' },
			{ propName: 'height', dynamicCssName: 'height' },
			{ propName: 'scaleX', transformationName: 'scaleX' },
			{ propName: 'scaleY', transformationName: 'scaleY' },
			{ propName: 'angleX', transformationName: 'rotateX' },
			{ propName: 'angleY', transformationName: 'rotateY' },
			{ propName: 'angleZ', transformationName: 'rotateZ' },
			{ propName: 'opacity', dynamicCssName: 'opacity' },
			{ propName: 'perspective', transformationName: 'perspective' },
			{ propName: 'skewX', transformationName: 'skewX' },
			{ propName: 'skewY', transformationName: 'skewY' },
		]);
	}
}

AnimatedTransform.displayName = 'AnimatedTransform';

AnimatedTransform.propTypes = {
	...GeneralAnimationPropTypes,
	angleX: PropTypes.number,
	angleY: PropTypes.number,
	angleZ: PropTypes.number,
};

export default AnimatedTransform;
