import PropTypes from 'prop-types';
import AnimateAction from './AnimateAction';
import { calculateMiddleValue } from './arithmetics';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import isDefined from '../util/isDefined';

class AnimateRotate extends AnimateAction {
	constructor(...args) {
		super(...args);
		this.className = 'AnimateRotate';
	}

	getAnimatedStyle(start, end, animationValue) {
		const {
			X: startX = 0,
			Y: startY = 0,
			Z: startZ = 0,
		} = start;
		const {
			X: endX,
			Y: endY,
			Z: endZ,
		} = end;

		const transformations = [];
		if (isDefined(endX)) {
			transformations.push({
				rotateX: calculateMiddleValue(startX, endX, animationValue),
			});
		}

		if (isDefined(endY)) {
			transformations.push({
				rotateY: calculateMiddleValue(startY, endY, animationValue),
			});
		}

		if (isDefined(endZ)) {
			transformations.push({
				rotateZ: calculateMiddleValue(startZ, endZ, animationValue),
			});
		}

		const style = {
			transform: transformations,
		};
		
		return style;
	}
}

AnimateRotate.displayName = 'AnimateRotate';

AnimateRotate.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
		Z: PropTypes.number,
	}),
	end: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
		Z: PropTypes.number,
	}),
};

export default AnimateRotate;
