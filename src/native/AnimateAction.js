import React from 'react';
import PropTypes from 'prop-types';
import { Animated, Easing } from 'react-native';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import log, { getLogId } from '../util/log';

class AnimateAction extends React.Component {
	constructor(...args) {
		super(...args);
		this.state = {};
		this.logId = getLogId();
	}

	componentDidMount() {
		
		this.startAnimation();
	}

	componentWillUnmount() {
		
		if (this.animation) {
			
			this.animation.stop();
			delete this.animation;
		}
	}

	logText = str => log(this.className, this.logId, str);

	startAnimation() {
		
		const { start, end, duration, useNativeDriver } = this.props;

		this.setState(
			{
				start,
				end,
				animationValue: new Animated.Value(0),
			},
			() => {
				
				this.animation = Animated.timing(
					this.state.animationValue,
					{
						toValue: 1,
						duration,
						easing: this.props.getEasingFunc ? this.props.getEasingFunc() : Easing.in(),
						useNativeDriver,
					},
				);
				this.animation.start(this.props.onFinished);
			},
		);
	}

	render() {
		const { animationValue, start, end } = this.state;
		
		if (!animationValue) {
			
			return null;
		}

		const { children, style = {} } = this.props;
		
		return (
			<Animated.View
				style={{
					...this.getAnimatedStyle(start, end, animationValue),
					...style,
				}}
				renderToHardwareTextureAndroid={this.props.renderToHardwareTextureAndroid}
			>
				{children}
			</Animated.View>
		);
	}
}

AnimateAction.propTypes = {
	...GeneralAnimationPropTypes,
	onFinished: PropTypes.func,
};

export default AnimateAction;
