import PropTypes from 'prop-types';
import AnimateAction from './AnimateAction';
import GeneralAnimationPropTypes from '../util/GeneralAnimationPropTypes';
import { calculateMiddleValue } from './arithmetics';
import isDefined from '../util/isDefined';

class AnimateMove extends AnimateAction {
	constructor(...args) {
		super(...args);
		this.className = 'AnimateMove';
	}

	getAnimatedStyle(start, end, animationValue) {
		const {
			top: startTop = 0,
			left: startLeft = 0,
		} = start;
		const {
			top: endTop,
			left: endLeft,
		} = end;
		const transformations = [];

		if (isDefined(endTop)) {
			transformations.push({
				translateY: calculateMiddleValue(startTop, endTop, animationValue),
			});
		}
		if (isDefined(endLeft)) {
			transformations.push({
				translateX: calculateMiddleValue(startLeft, endLeft, animationValue),
			});
		}
		const style = {
			position: 'absolute',
			transform: transformations,
		};
		
		return style;
	}
}

AnimateMove.displayName = 'AnimateMove';

AnimateMove.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		top: PropTypes.number,
		left: PropTypes.number,
	}),
	end: PropTypes.shape({
		top: PropTypes.number,
		left: PropTypes.number,
	}),
};

export default AnimateMove;
