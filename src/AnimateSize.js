import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';
import AnimateAction from './AnimateAction';

class AnimateMove extends AnimateAction {
	constructor(...args) {
		super(['width', 'height'], ...args);
	}

	getStyle(position) {
		const { width, height } = position;
		const coordinates = 
			[
				['width', width],
				['height', height],
			]
			.reduce(
				(coordinates, [coordinateName, value]) =>
					isDefined(value) ? {
						...coordinates,
						[coordinateName]: `${value}px`,
					} : coordinates,
				{},
			);

		return {
			...coordinates,
		};
	}
}

AnimateMove.displayName = 'AnimateMove';

AnimateMove.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		top: PropTypes.number,
		left: PropTypes.number,
	}),
	end: PropTypes.shape({
		top: PropTypes.number,
		left: PropTypes.number,
	}),
};

export default AnimateMove;
