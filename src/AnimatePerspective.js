import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';
import AnimateAction from './AnimateAction';

class AnimatePerspective extends AnimateAction {
	constructor(...args) {
		super(['transform'], ...args);
	}

	getStyle(perspective) {
		return {
			transform: isDefined(perspective) ? `perspective(${perspective}px)` : 'none',
		};
	}
}

AnimatePerspective.displayName = 'AnimatePerspective';

AnimatePerspective.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.number,
	end: PropTypes.number,
};

export default AnimatePerspective;
