import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';
import AnimateAction from './AnimateAction';

class AnimateScale extends AnimateAction {
	constructor(...args) {
		super(['transform'], ...args);
	}

	getStyle(scaleFactor) {
		const { X, Y } = scaleFactor;
		const transformations = 
			[
				['scaleX', X],
				['scaleY', Y],
			]
			.reduce(
				(transformations, [transformName, value]) =>
					isDefined(value) ? `${transformations} ${transformName}(${value})` : transformations,
				'',
			);
		return {
			transform: transformations || 'none',
		};
	}
}

AnimateScale.displayName = 'AnimateScale';

AnimateScale.propTypes = {
	...GeneralAnimationPropTypes,
	start: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
	}),
	end: PropTypes.shape({
		X: PropTypes.number,
		Y: PropTypes.number,
	}),
};

export default AnimateScale;
