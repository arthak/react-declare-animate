import React from 'react';
import PropTypes from 'prop-types';
import isDefined from './util/isDefined';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

const AnimatedPerspective = ({
	perspective,
	duration,
	children,
	getEasingFunc,
	style = {},
	...customProps
}) => (
	<div
		style={{
			transform: isDefined(perspective) ? `perspective(${perspective}px)` : 'none',
			transition: `transform ${duration / 1000}s`,
			transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
			...style,
		}}
		{...customProps}
	>
		{children}
	</div>
);

AnimatedPerspective.displayName = 'AnimatedPerspective';

AnimatedPerspective.propTypes = {
	...GeneralAnimationPropTypes,
	perspective: PropTypes.number,
};

export default AnimatedPerspective;
