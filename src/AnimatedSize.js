import React from 'react';
import PropTypes from 'prop-types';
import GeneralAnimationPropTypes from './util/GeneralAnimationPropTypes';

const AnimatedSize = ({ width, height, duration, children, getEasingFunc, style = {}, ...customProps }) => (
	<div
		style={{
			width,
			height,
			transition: `width ${duration / 1000}s, height ${duration / 1000}s`,
			transitionTimingFunction: getEasingFunc ? getEasingFunc() : 'ease',
			...style,
		}}
		{...customProps}
	>
		{children}
	</div>
);

AnimatedSize.displayName = 'AnimatedSize';

AnimatedSize.propTypes = {
	...GeneralAnimationPropTypes,
	width: PropTypes.number,
	height: PropTypes.number,
};

export default AnimatedSize;
